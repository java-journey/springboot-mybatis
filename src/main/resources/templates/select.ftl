<#macro select id datas>
    <select id="${id}" name="${id}">
        <option>---请选择---</option>
        <#list datas as data>
            <option value="${data.typeId}">${data.name}</option>
        </#list>
    </select>
</#macro>

<#macro select id list listKey listValue path emptyOption=true attributes="" emptyValue="">
    <@spring.bind path/>
    <select id="${id}" name="${spring.status.expression}" ${attributes}>
        <#if emptyOption><option value=''>${emptyValue}</option></#if>
        <#list list as item>
            <option value='${item[listKey]}' <@spring.checkSelected item[listKey]/>>${item[listValue]}</option><#rt/>
        </#list>
    </select>
</#macro>
