package com.springboot.mybatis.interceptor;

import com.springboot.mybatis.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

@Slf4j
public class RestTemplateTraceIdInterceptor implements ClientHttpRequestInterceptor {
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution chrExecution) {
        String traceId = MDC.get(Constants.TRACE_ID);
        ClientHttpResponse c = null;
        assert httpRequest != null : "httpRequest is null";
        assert bytes != null : "bytes is null";
        assert chrExecution != null : "clientHttpRequestExecution is null";

        if (traceId != null) {
            httpRequest.getHeaders().add(Constants.TRACE_ID, traceId);
        }
        try {
            c = chrExecution.execute(httpRequest, bytes);
        } catch (Exception e) {
            log.error(" Exception {} :", e.getMessage());
            e.printStackTrace();
        }

        return c;
    }


/**
 *客户端调用
 */

   // restTemplate.setInterceptors(Arrays.asList(new RestTemplateTraceIdInterceptor()));
}
