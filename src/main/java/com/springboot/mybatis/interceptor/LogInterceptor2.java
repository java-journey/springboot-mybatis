package com.springboot.mybatis.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * 日志自定义拦截类，将日志操作加入全局mdc
 *
 *
 *  @author sol
 *  @Version 1.0
 */
@Component
public class LogInterceptor2 implements HandlerInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogInterceptor2.class);
    /**
     * 设备
     */
    private static final String USER_AGENT = "user-agent";
    /**
     * 位置定义
     */
    private static final String UNKNOWN = "unknown";
    /**
     * 本地ip
     */
    private static final String LOCAL_IP = "0:0:0:0:0:0:0:1";
    /**
     * 全局mdc
     */
    private static final String TRACE_ID = "traceId";

    /**
     * 全局ip
     */
    private static final String IP = "ip";

    /**
     * 全局url
     */
    private static final String URL = "url";


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                             Object object) throws Exception {
        // 定义一个全局的tranceId 使用32为uuid+随机6位数字
        String traceId = UUID.randomUUID().toString().replaceAll("-", "");
        // 放入日志全局id
        MDC.put(TRACE_ID, traceId);
        // 放入ip
        MDC.put(IP, getIpAddr(httpServletRequest));
//        LOGGER.info("put traceId ({}) to logger", traceId);
        // 获取url地址
        String url = httpServletRequest.getRequestURI();
        // 放入设备
        MDC.put(URL,url);
        // 日志打印 地址--设备--访问接口和访问ip
//        LOGGER.info(url);
//        LOGGER.info("UserAgent: {}", httpServletRequest.getHeader(USER_AGENT));
//        LOGGER.info("get request interface: {}, get ip address: {}, UserAgent: {}", url, getIpAddr(httpServletRequest),httpServletRequest.getHeader(USER_AGENT));
        //将traceId返回给前端
        httpServletResponse.setHeader(TRACE_ID,traceId);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                           Object object, ModelAndView modelAndView) throws Exception {
        //执行成功，获取日志全局id
       String traceId = MDC.get(TRACE_ID);
       LOGGER.info("remove traceId ({}) from logger", traceId);
        //删除当前操作全局id
       // MDC.remove(TRACE_ID);
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                Object object, Exception e) throws Exception {
        // 自定义
        MDC.remove(TRACE_ID);
    }

    /**
     * 根据request请求获取ip
     *
     * @param request
     *            请求
     * @return ip 获取的ip
     */
    private String getIpAddr(HttpServletRequest request) {
        // 获取请求头信息
        String ip = request.getHeader("x-forwarded-for");
//        LOGGER.info("get all ip:" + ip);
        // 取不到值 执行不同请求头信息
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        // 如果本地调用 默认返回 127.0.0.1
        if (LOCAL_IP.equals(ip)) {
            ip = "127.0.0.1";
        }
        // 如果是多级代理，取第一个ip为客户真实ip
        if (ip != null && ip.split(",").length > 1) {
            ip = ip.split(",")[0];
        }
        return ip;
    }

}
