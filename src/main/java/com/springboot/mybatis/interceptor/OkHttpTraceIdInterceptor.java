package com.springboot.mybatis.interceptor;

import com.springboot.mybatis.utils.Constants;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.MDC;

import java.io.IOException;

/**
 * 实现OKHttp拦截器：
 */
public class OkHttpTraceIdInterceptor implements Interceptor {
    /**
     *  实现Interceptor拦截器，重写interceptor方法，
     *  实现逻辑和HttpClient差不多，
     *  如果能够获取到当前线程的traceId则向下透传
     */
    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        String traceId = MDC.get(Constants.TRACE_ID);
        Request request = null;
        if (traceId != null) {
            //添加请求体
            request = chain.request().newBuilder().addHeader(Constants.TRACE_ID, traceId).build();
        }
        Response originResponse = chain.proceed(request);

        return originResponse;
    }

    /**
     * 提供给OkHttp 客户端参考
     * 为OkHttp添加拦截器：调用addNetworkInterceptor方法添加拦截器
     */
    private static OkHttpClient client = new OkHttpClient.Builder()
            .addNetworkInterceptor(new OkHttpTraceIdInterceptor())
            .build();
}
