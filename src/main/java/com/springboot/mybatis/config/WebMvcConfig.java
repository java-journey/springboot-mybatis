package com.springboot.mybatis.config;

import com.springboot.mybatis.interceptor.LogInterceptor2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author sol
 * @version 1.0
 **/
@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private LogInterceptor2 logInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        log.info(" WebMvcConfig.logInterceptor begin{0}");
        registry.addInterceptor(logInterceptor);
        WebMvcConfigurer.super.addInterceptors(registry);

        log.info(" WebMvcConfig.logInterceptor end{0}");
    }

}