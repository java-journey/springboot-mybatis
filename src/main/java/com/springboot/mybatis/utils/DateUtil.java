
package com.springboot.mybatis.utils;



import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author sol
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DateUtil {
    private DateUtil() {
    }


    private static final Logger LOG = LoggerFactory.getLogger(DateUtil.class);

    /**
     * 格式 yyyy-MM-dd HH:mm:ss
     */
    private static final DateTimeFormatter DATEFORMATE_YYYYMMDDHHMMSS_FAST = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    static final DateTimeFormatter YYYYMMDD_FORMAT_FAST = DateTimeFormat.forPattern(DateConstant.DATEFORMATE_YYYYMMDD);


    /**
     * 格式 yyyy-MM-dd
     */
    private static final DateTimeFormatter DATEFORMATE_FAST = DateTimeFormat.forPattern("yyyy-MM-dd");

    static final DateFormat YYYYMMDD_FORMAT = new SimpleDateFormat(DateConstant.DATEFORMATE_YYYYMMDD);

    /**
     * 一天中最大时间戳
     */
    private static final int MILLIS_PER_DAY = DateTimeConstants.MILLIS_PER_DAY - 1;

    /**
     * 查询条件向前推30天
     */
    private static final int BEFORE_WEEK_DAY = 30;

    /**
     * 日期格式转化：yyyyMMdd
     *
     * @param date
     * @return yyyyMMdd
     */
    public static synchronized String formatDateYYYYMMDD(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        return dateFormat.format(date);
    }


    /**


    /**
     * 当前日期格式转化：yyyyMMdd
     *
     * @return yyyyMMdd
     */
    public static String formatNowYYYYMMDD() {
        return formatDateYYYYMMDD(new Date());
    }

    /**
     * 日期格式转化：HHmmss
     *
     * @param date
     * @return HHmmss
     */
    public static String formatDateHHMMSS(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("HHmmss");
        return dateFormat.format(date);
    }

    /**
     * 把yyyyMMdd格式字符串转换成 java.util.Date
     *
     * @param datestr
     * @return
     */
    public static Date getUtilDateByShortStr(String datestr) {
        DateTime dateTime = YYYYMMDD_FORMAT_FAST.parseDateTime(datestr);
        return dateTime.toDate();
    }

    /**
     * 当前日期格式转化：HHmmss
     *
     * @return HHmmss
     */
    public static String formatNowHHMMSS() {
        return formatDateHHMMSS(new Date());
    }

    /**
     * 字符串格式：yyyyMMdd转日期
     *
     * @param dateStr
     * @return
     */
    public static synchronized Date parseDateYYYYMMDD(String dateStr) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * months为正往后,
     * mounts为负往前
     * @param date
     * @param months
     * @return
     */
    public static Date addMounth(Date date, int months){
        return new DateTime(date).plusMonths(months).toDate();
    }

    /**
     * 字符串格式：yyyyMMddHHmmss转日期
     *
     * @param dateStr
     * @return
     */
    public static Date parseDateYYYYMMDDHHMMSS(String dateStr) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }
     /**
       * 功能描述: <br>
       *<joda 计算两个日期相差几天>
       *
       * @param
       * @return
       */
    public static int getBetweenDay(Date start, Date end) {
        DateTime sDateTime = new DateTime(start);
        DateTime eDateTime = new DateTime(end);
        int days = Days.daysBetween(sDateTime, eDateTime).getDays();
        return days;
    }
    /**
     * 得到几天后的时间
     *
     * @param d
     * @param day
     * @return
     */
    public static Date getDateAfter(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
        return now.getTime();
    }

    /**
     * 得到几小时前的时间
     *
     * @param d
     * @param hours
     * @return
     */
    public static Date getDateTimeBeforeHours(Date d, int hours) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY) - hours);
        return now.getTime();
    }

    /**
     * 把yyyy-MM-dd格式的字符串转换成Date
     *
     * @param dateStr
     * @return
     */
    public static synchronized Date getDateOfStr(String dateStr, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date da = null;
        try {
            da = df.parse(dateStr);
        } catch (Exception e) {
            LOG.error("getDateOfStr exception{} ", e);
        }
        return da;
    }

    /**
     * 把yyyy-MM-dd HH:mm:ss格式字符串转换成 java.util.Date
     *
     * @param datestr
     * @return
     */
    public static Date getUtilDateByLongStr(String datestr) {
		if (StringUtils.isEmpty(datestr)) {
			return null;
		}
        DateTime dateTime = DATEFORMATE_YYYYMMDDHHMMSS_FAST.parseDateTime(datestr);
        return dateTime.toDate();
    }


    /**
     * 把yyyy-MM-dd格式字符串转换成 java.util.Date
     *
     * @param datestr
     * @return
     */
    public static Date getUtilDateByStr(String datestr) {
        if (StringUtils.isEmpty(datestr)) {
            return null;
        }
        DateTime dateTime = DATEFORMATE_FAST.parseDateTime(datestr);
        return dateTime.toDate();
    }

    /**
     * 获取当前时间
     *
     * @return Date
     */
    public static Date getCurDate() {
        return new DateTime().toDate();
    }

    /**
     * 功能描述:根据指定格式，格式化日期 <br>
     * 〈功能详细描述〉
     *
     * @param date
     * @param format
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static String formatDate(Date date, String format) {
        if (null == date) {
            return "";
        } else {
            return new DateTime(date).toString(DateTimeFormat.forPattern(format));
        }
    }

    /**
     * 功能描述:根据指定格式，格式化日期 <br>
     * 〈功能详细描述〉
     *
     * @param date
     * @return yyyy-MM-dd
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static String formatDateForYYYYMMDD(Date date) {
        if (null == date) {
            return "";
        } else {
            return new DateTime(date).toString(DateTimeFormat.forPattern(DateConstant.DATEFORMATE_YYYY_MM_DD));
        }
    }

    /**
     * 获取所传时间的开始时间 <br>
     * 例如：入参是2014-09-11 18:09:39，返回为2014-09-11 00:00:00
     *
     * @param startDate
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static Date getStartDate(String startDate) {
        return DATEFORMATE_FAST.parseDateTime(startDate).withMillisOfDay(0).toDate();
    }

    /**
     * 获取所传时间的结束时间 <br>
     * 例如：入参是2014-09-11 18:09:39，返回为2014-09-11 23:59:59
     *
     * @param endDate
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static Date getEndDate(String endDate) {
        return DATEFORMATE_FAST.parseDateTime(endDate).withMillisOfDay(MILLIS_PER_DAY).toDate();
    }

    /**
     * 获取 yyyy-MM-dd格式的字符串<br>
     * 〈功能详细描述〉
     *
     * @param date
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static String getShortFormatDateStr(Date date) {
        return formatDate(date, "yyyy-MM-dd");
    }

    /**
     * 功能描述:yyyy-MM-dd HH:mm:ss格式化时间 <br>
     *
     * @param date
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static String formatDate(Date date) {
        if (null == date) {
            return "";
        }
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String formatDateStr(Date date) {
        return formatDate(date, DateConstant.DATEFORMATE_YYYY_MM_DD);
    }

    public static String formatTimeStr(Date date) {
        return formatDate(date, DateConstant.TRADE_TIMEFORMATE);
    }
    /**
     * 将日期转换成字符串
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String dateToString(Date date, String pattern) {
        if (date != null && org.apache.commons.lang3.StringUtils.isNotBlank(pattern)) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        } else {
            return null;
        }
    }

    /**
     * 得到几天前的时间
     *
     * @param d
     * @param day
     * @return
     */
    public static Date getDateBefore(Date d, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        return now.getTime();
    }

    /**
     * 返回当前时间前七天日期
     * 格式 yyyy-MM-dd
     *
     * @return
     */
    public static String getStartTransDate() {
        Date now = DateUtil.getCurDate();
        return DateUtil.formatDate(DateUtil.getDateBefore(now, BEFORE_WEEK_DAY),
                DateConstant.DATEFORMATE_YYYY_MM_DD);
    }

    /**
     * 返回当前日期
     * 格式 yyyy-MM-dd
     *
     * @returns
     */
    public static String getEndTransDate() {
        return DateUtil.formatDate(DateUtil.getCurDate(), DateConstant.DATEFORMATE_YYYY_MM_DD);
    }

    /**
     * 功能描述: <br>
     * 〈以yyyyy-MM-dd格式 获取当前时间〉
     *
     * @return
     * @see [相关类/方法](可选)
     * @since [产品/模块版本](可选)
     */
    public static String getCurrentDateString() {
        return new DateTime().toString(DateConstant.DATEFORMATEyyyyMMddHHmmss);
    }

    /**
     * 把日期转换成 yyyyMMdd格式的字符串
     *
     * @param date
     * @return
     */
    public synchronized static String getShortStrDate(Date date) {
        return YYYYMMDD_FORMAT.format(date);
    }

    /**
     * 获得时分格式的时间
      * @param checkTime
     * @return
     */
    public static Date getCheckDate(String checkTime){
        String yyyyMMdd = DateUtil.formatDate(DateUtil.getCurDate(), DateConstant.DATEFORMATE_YYYY_MM_DD);
        String checkedTime;
        if(StringUtils.isEmpty(checkTime)){
            checkedTime = DateConstant.DEFAULT_RECON_TIME;
        }else{
            checkedTime = checkTime;
        }
        String checkDateTime = yyyyMMdd + " " + checkedTime;
        return DateUtil.getDateOfStr(checkDateTime, DateConstant.DATEFORMATE_YYYY_MM_DD_HH_MM);
    }


    public static String getFirstDate(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal_1=Calendar.getInstance();
        cal_1.set(Calendar.DAY_OF_MONTH,1);
        String firstDate = format.format(cal_1.getTime());
        return firstDate;
    }

    public static String getLastDate(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal_1=Calendar.getInstance();
        cal_1.set(Calendar.DAY_OF_MONTH,30);
        String firstDate = format.format(cal_1.getTime());
        return firstDate;
    }
     /**
       * 功能描述: <br>
       *<获取当前时间戳>
       *
       * @param
       * @return
       */
    public static String getTimeStamp(){
        return formatDate(new Date(),"yyyyMMddHHmmssSSS");
    }

    /**
     *
     * @return
     */
    public static Time toTimeHHmm(String strTime){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date d = null;
        try {
            d = format.parse(strTime);
        } catch (Exception e) {
            LOG.error("转时间HHmm异常",strTime,e);
        }
        if(d != null) {
            Time time = new Time(d.getTime());
            return time;
        }
        return null;
    }

    /**
     * 比较2个时间
      * @param date1 yyyyMMdd
     * @param date2 yyyyMMdd
     * @return
     */
    public static boolean after(String date1, String date2){
        //比较2个时间
        Date dateD1 = getDateOfStr(date1, DateConstant.DATEFORMATE_YYYYMMDD);

        Date dateD2 = getDateOfStr(date2, DateConstant.DATEFORMATE_YYYYMMDD);

        return dateD1.after(dateD2);
    }

    /**
     *
     * @param date1 - 靠近现在的时间
     * @param date2 - 久远的时间
     * @return
     */
    public static long getMinutes(Date date1, Date date2){
        Calendar dateOne=Calendar.getInstance();
        Calendar dateTwo=Calendar.getInstance();
        dateOne.setTime(date1);    //设置为当前系统时间
        dateTwo.setTime(date2);    //获取数据库中的时间
        long timeOne=dateOne.getTimeInMillis();
        long timeTwo=dateTwo.getTimeInMillis();
        long minute=(timeOne - timeTwo) / (1000 * 60);//转化minute

        return minute;
    }

    public static final String parseDateToStr(final String format, final Date date) {
        return new SimpleDateFormat(format).format(date);
    }


    /**
     * 得到几个月前
     *
     * @param d
     * @param month
     * @return
     */
    public static Date getDateBeforeonth(Date d, int month) {
        Calendar calendar = Calendar.getInstance(); //得到日历
        calendar.setTime(d);//把当前时间赋给日历
        calendar.add(calendar.MONTH, -month);
        return calendar.getTime();
    }
    public static Date getStringToDate(String time) throws ParseException {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        Date date = ft.parse(time);
        return date;
    }


    /**
     * 根据时间范围获得月份集
     *
     * @return
     */
    public static List<String> getRangeSet(String beginDate, String endDate) {

        List<String> rangeSet = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        SimpleDateFormat sdfYYYYMM = new SimpleDateFormat("yyyyMM");
        try {
            Date begin_date = sdf.parse(beginDate);//定义起始日期
            Date end_date = sdf.parse(endDate);//定义结束日期

            Calendar dd = Calendar.getInstance();//定义日期实例
            dd.setTime(begin_date);//设置日期起始时间
            while (!dd.getTime().after(end_date)) {//判断是否到结束日期
                rangeSet.add(sdfYYYYMM.format(dd.getTime()));
                dd.add(Calendar.MONTH, 1);//进行当前日期月份加1
            }
        } catch (ParseException e) {
            LOG.error("getRangeSet转时间HHmm异常{}", e);
        }
        return rangeSet;
    }


    public static boolean flagIn26DayDate(String str) {
        String YYYY_MM_DD = "yyyy-MM-dd";
        int day = -25;
        boolean convertSuccess = false;
        //时间格式定义
        SimpleDateFormat format = new SimpleDateFormat(YYYY_MM_DD);
        //获取当前时间日期--nowDate
        String nowDate = format.format(new Date());
        //获取26天前的时间日期--minDate
        Calendar calc = Calendar.getInstance();
        calc.add(Calendar.DAY_OF_MONTH, day);
        String minDate = format.format(calc.getTime());
        try {
            //设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            //获取字符串转换后的时间--strDate
            String strDate = format.format(format.parse(str));
            if (nowDate.compareTo(strDate) >= 0 && strDate.compareTo(minDate) >= 0){
                convertSuccess = true;
            }
        } catch (ParseException e) {
            LOG.error("flagIn30DayDate转时间HHmm异常{}", e);
        }
        return convertSuccess;
    }

}