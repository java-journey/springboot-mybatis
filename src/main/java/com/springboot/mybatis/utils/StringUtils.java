/*
 * Copyright (C),
 * FileName:
 * Author:
 * Date:
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名     修改时间    版本号       描述
 */
package com.springboot.mybatis.utils;

import java.text.FieldPosition;
import java.text.NumberFormat;

import static java.text.NumberFormat.INTEGER_FIELD;

public class StringUtils {

    public static boolean isEmpty(String string) {
        return null == string || string.length() == 0;
    }

    public static String leftFillZero(String s, int length) {
        if (StringUtils.isEmpty(s) || length <= 0) {
            return "";
        }
        if (s.length() == length) {
            return s;
        }
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < length) {
            sb.append("0");
            i++;
        }
        sb.append(s);
        return sb.substring(sb.length() - length);
    }


    public static String generator(int number, int mumIntegerDigits, String prefix) {

        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置最小整数位数
        numberFormat.setMinimumIntegerDigits(mumIntegerDigits);
        // 去掉逗号
        numberFormat.setGroupingUsed(false);
        // 前缀
        StringBuffer buffer = new StringBuffer();
        buffer.append(prefix);

        return numberFormat.format(number, buffer, new FieldPosition(INTEGER_FIELD)).toString();
    }

    public static String leftFillZero( int length,long num){

        String  tem = "%0"+length+"d";

        tem = String.format(tem, num);


        return tem;

    }



    public static void main(String[] args) {
        // 0代表前面要补位的字符、6代表字符串的长度、d表示参数为整数类型,9996为 int 类型、
        String str = String.format("%06d", 9996);
        System.out.println(str);


        String str2 = leftFillZero(9,1980);
        System.out.println(str2);


        // 首先通过 getInstance 创建一个 NumberFormat 实例
        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置最大和最小整数位数
        numberFormat.setMaximumIntegerDigits(2);
        numberFormat.setMinimumIntegerDigits(2);
        // 最后通过 format 方法操作要补零的数字即可
        System.out.println(numberFormat.format(2));

    }
}
