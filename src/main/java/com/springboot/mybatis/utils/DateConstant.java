/*
* Copyright (C), 2002-2016, 苏宁易购电子商务有限公司
* FileName: DateConstant.java
* Author:   17092224
* Date:     2018/7/6  18:01
* Description: //模块目的、功能描述      
* History: //修改记录
* <author>      <time>      <version>    <desc>
* 修改人姓名          修改时间            版本号                  描述
*/
package com.springboot.mybatis.utils;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author 17092224
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class DateConstant {
    /**
     * 年月日常量
     */
    public final static String DATEFORMATE_YYYYMMDD = "yyyyMMdd";

    public final static String DATEFORMATE_YYYYMM = "yyyyMM";


    public final static String DATEFORMATE_YYYY_MM_DD = "yyyy-MM-dd";

    public final static String DATEFORMATE_YYYY_MM = "yyyy-MM";

    /**
     * 年月日时分秒
     */
    public final static String DATEFORMATE_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    public static final String DATEFORMATE_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public final static String DATEFORMATEyyyyMMddHHmmss ="yyyy-MM-dd HH:mm:ss";

    public final static String DEFAULT_RECON_TIME = "04:00";

    public final static String DATEFORMATE_YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";

    public final static String DATEFORMATE_YYYYMMDD_HH_MM = "yyyyMMdd HH:mm";

    public final static String DATEFORMATE_YYYYMMDD_2 = "yyyy/MM/dd";

    /**
     * hhmmss时间格式
     */
    public static final String TRADE_TIMEFORMATE="HH:mm:ss";

    public static final String MMDD_HHMM = "MMdd HH:mm";

    public static final String DD_HHMM = "dd HH:mm";

    private DateConstant(){}

}