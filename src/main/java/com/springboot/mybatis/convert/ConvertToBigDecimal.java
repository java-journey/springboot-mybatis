package com.springboot.mybatis.convert;

import com.opencsv.bean.AbstractBeanField;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 *
 * @author sol
 * @create 2019-11-23 13:20
 */
public class ConvertToBigDecimal extends AbstractBeanField {
    @Override
    protected Object convert(String value) {
        if(StringUtils.isNotBlank(value)) {
            return new BigDecimal(value);
        }
        return new BigDecimal(0);
    }
}
