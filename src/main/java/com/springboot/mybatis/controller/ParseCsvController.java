package com.springboot.mybatis.controller;

//import cn.hutool.core.util.CharsetUtil;

import cn.hutool.core.util.CharsetUtil;
import com.opencsv.bean.*;
import com.springboot.mybatis.domain.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author sol
 *  2019-11-14 15:52
 */
@RestController
@RequestMapping("/csv")
public class ParseCsvController {

    @PostMapping("/parseByName")
    public List parseByName(MultipartFile file) throws IOException {
        Reader inputStream = new InputStreamReader(file.getInputStream(), CharsetUtil.CHARSET_GBK);
        // 设置解析策略，csv的头和POJO属性的名称对应，也可以使用@CsvBindByName注解来指定名称
        HeaderColumnNameMappingStrategy<CarCsvDTOByName> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(CarCsvDTOByName.class);

        CsvToBeanBuilder csvToBeanBuilder = new CsvToBeanBuilder(inputStream);
        csvToBeanBuilder.withMappingStrategy(strategy);
        CsvToBean csvToBean = csvToBeanBuilder
                .build();
        AtomicReference<List> carCsvDTOList = new AtomicReference<>(csvToBean.parse());
        return carCsvDTOList.get();
    }

    @PostMapping("/parseByPosition")
    public List parseByPosition(String filePath) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(Files.newInputStream(Paths.get(filePath)), CharsetUtil.CHARSET_GBK);
        // 设置解析策略，使用@CsvBindByPosition注解可以指定字段在csv文件头中的位置，从0开始
        ColumnPositionMappingStrategy<CarCsvDTOByPosition> strategy = new ColumnPositionMappingStrategy<>();
        strategy.setType(CarCsvDTOByPosition.class);

        CsvToBean csvToBean = new CsvToBeanBuilder(inputStream)
                .withMappingStrategy(strategy)
                .build();
        List carCsvDTOList = csvToBean.parse();
        return carCsvDTOList;
    }

    @PostMapping("/parseByMappingArray")
    public List parseByMappingArray(String filePath) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(Files.newInputStream(Paths.get(filePath)), CharsetUtil.CHARSET_GBK);
        // 设置解析策略，csv文件不需要头，由程序指定
        ColumnPositionMappingStrategy<CarCsvDTOByMappingArray> strategy = new ColumnPositionMappingStrategy<CarCsvDTOByMappingArray>();
        strategy.setType(CarCsvDTOByMappingArray.class);
        String headers = "id|shortName|name|remark|parentId|typeName|typeId";
        String[] headerArr = headers.split("\\|");
        strategy.setColumnMapping(headerArr);

        CsvToBean csvToBean = new CsvToBeanBuilder(inputStream)
                .withMappingStrategy(strategy)
                .build();
        List carCsvDTOList = csvToBean.parse();
        return carCsvDTOList;
    }

    @PostMapping("/parseByMappingByTranslate")
    public List parseByMappingByTranslate(String filePath) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(Files.newInputStream(Paths.get(filePath)), CharsetUtil.CHARSET_GBK);
        // 设置解析策略，key-csv的头、value-DTO属性
        HeaderColumnNameTranslateMappingStrategy<CarCsvDTOByTranslate> strategy = new HeaderColumnNameTranslateMappingStrategy<>();
        strategy.setType(CarCsvDTOByTranslate.class);
        Map<String, String> columnMapping = new HashMap<>();
        columnMapping.put("id", "id");
        columnMapping.put("short_name", "shortName");
        columnMapping.put("name", "name");
        columnMapping.put("remark", "remark");
        columnMapping.put("parent_id", "parentId");
        columnMapping.put("type_name", "typeName");
        columnMapping.put("type_id", "typeId");
        strategy.setColumnMapping(columnMapping);

        CsvToBean csvToBean = new CsvToBeanBuilder(inputStream)
                .withMappingStrategy(strategy)
                .build();
        List carCsvDTOList = csvToBean.parse();
        return carCsvDTOList;
    }

    @PostMapping("/convertAndValid")
    public List convertAndValid(String filePath) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(Files.newInputStream(Paths.get(filePath)), CharsetUtil.CHARSET_GBK);
        HeaderColumnNameMappingStrategy<CarCsvDTOConvertAndValid> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(CarCsvDTOConvertAndValid.class);
        // 校验必输项以及做类型转换
        CsvToBean csvToBean = new CsvToBeanBuilder(inputStream)
                .withMappingStrategy(strategy)
                .build();
        List carCsvDTOList = csvToBean.parse();
        return carCsvDTOList;
    }

    @PostMapping("/parseBySelf")
    public List parseBySelf(String filePath) throws IOException {
        InputStreamReader inputStream = new InputStreamReader(Files.newInputStream(Paths.get(filePath)), CharsetUtil.CHARSET_GBK);
        HeaderColumnNameMappingStrategy<CarCsvDTOConvertAndValid> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(CarCsvDTOConvertAndValid.class);

        CsvToBean csvToBean = new CsvToBeanBuilder(inputStream)
                .withSkipLines(2) // 跳过行数
                .withSeparator('|') // 分隔符
               // .withFilter(new SkipLineFilter())
                .withThrowExceptions(false) // 如果有必输项没有，则不抛异常忽略此行
                .withMappingStrategy(strategy)
                .build();
        List carCsvDTOList = csvToBean.parse();
        return carCsvDTOList;
    }

}