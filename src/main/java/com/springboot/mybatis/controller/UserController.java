package com.springboot.mybatis.controller;

import com.springboot.mybatis.domain.User;
import com.springboot.mybatis.service.AccountService;
import com.springboot.mybatis.service.EmployService;
import com.springboot.mybatis.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @Autowired
    private AccountService accountService;

    @Autowired
    private EmployService employService;

    @GetMapping(value = "/userList")
    public String userList(Model model) {
        User user = new User();
        user.setName("逍遥子");
        user.setAge(28);
        user.setId(1);

        List< User> userList = new ArrayList<>();
        userList.add(user);
        model.addAttribute("userList",userList);
        return "/user/list";
    }

    //查询所有
    @RequestMapping("/getAllList")
    public List<User> getUserList(){
        return userService.getAllList();
    }


    @GetMapping("/getId")
    public String getSquence(@RequestParam("id") String id){
        System.out.println("id :"+id);
        if(!StringUtils.isEmpty(id)){
            return accountService.generateBankAccountNo(id);
        }
        return "/index";
    }

    @GetMapping("/getId2")
    public String getSquence2(@RequestParam("id") String id){
        System.out.println("id :"+id);
        if(!StringUtils.isEmpty(id)){
            return accountService.generateBankAccountNo2(id);
        }
        return "/index";
    }


    //通过主键Id查询
    @RequestMapping("/getOne/{id}")
    public User getUserById(@PathVariable String id){
        return userService.getUserById(id);
    }

    //新增用户
    @RequestMapping("/save")
    public String saveUser(@RequestBody User user){
        userService.saveUser(user);
        return "SUCCESS";
    }

    //修改用户
    @RequestMapping("/update")
    public String editUser(@RequestBody User user){
        userService.editUser(user);
        return "SUCCESS";
    }


    /**
     *删除用户
     * @author sol
     * @date 2022/11/20 9:47
     * @param id
     * @return java.lang.String
     */
    @RequestMapping("/deleteOne/{id}")
    public String deleteUser(@PathVariable String id){
        userService.deleteUser(id);
        return "SUCCESS";
    }

}