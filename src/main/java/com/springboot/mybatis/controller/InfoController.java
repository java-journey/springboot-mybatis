package com.springboot.mybatis.controller;

import com.springboot.mybatis.domain.AccountType;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.*;

/**
 * @ClassName:InfoController
 * @Description TODO
 * @Author sol
 * @Date 2021/6/15 10:55
 * @Version 1.0
 **/
@Controller
public class InfoController {

    @RequestMapping("/index")
    public String info(Model model){
        model.addAttribute("web","FreeMarker展示信息");
        model.addAttribute("user","test");
        Map<String,Object> info=new HashMap<>();
        info.put("name","sol");
        info.put("url","http://www.bing.com");
        model.addAttribute("info",info);
        model.addAttribute("role","role");
        model.addAttribute("list",getAccountTypeList());
        return "index";
    }
    private List<AccountType> getAccountTypeList() {
        List<AccountType> userList = new ArrayList<>();
        // for(int i=1;i<=4;i++){
        AccountType type = new AccountType();
        type.setName("TA清算账户");
        type.setId("01");
        type.setDescription("一个简单的描述");
        userList.add(type);

        AccountType type2 = new AccountType();
        type2.setName("募集清算账户");
        type2.setId("02");
        type2.setDescription("一个简单的描述");
        userList.add(type2);

        AccountType type3 = new AccountType();
        type3.setName("销售清算账户");
        type3.setId("03");
        type3.setDescription("一个简单的描述");
        userList.add(type3);

        AccountType type4 = new AccountType();
        type4.setName("中登交易账户");
        type4.setId("04");
        type4.setDescription("一个简单的描述");
        userList.add(type4);
        // }
        return userList;
    }


    @PostMapping("add")
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@RequestBody AccountType person) {
        // ...
        System.out.print(" add "+person.getName());
    }



    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public List<AccountType> query(@RequestBody AccountType person) {
        // ...
        List<AccountType> list = new ArrayList<>();
        System.out.print(" add "+person.getName());

        return list;
    }
}
