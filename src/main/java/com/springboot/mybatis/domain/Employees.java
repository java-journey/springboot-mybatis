package com.springboot.mybatis.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Employees {
    private Integer employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Date hireDate;
    private String jobId;
    private BigDecimal salary;
    private BigDecimal commissionPct;
    private Integer managerId;
    private Short departmentId;
}
