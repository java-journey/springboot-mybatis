package com.springboot.mybatis.domain;

import lombok.Data;

@Data
public class UserVO {

    private int id;
    private String name;
}
