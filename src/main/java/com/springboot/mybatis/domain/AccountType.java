package com.springboot.mybatis.domain;
import lombok.Data;

/**
 *
 * @author sol
 * @date 2022/11/19 22:15 
  */
@Data
public class AccountType {

    private String id;
    private String name;
    private String description;




}
