/*
 * Copyright (C), 2002-,
 * FileName:
 * Author:
 * Date:
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名     修改时间    版本号       描述
 */
package com.springboot.mybatis.service;

public interface AccountService {

    /**
     *
     * @author 12010061
     * @date 2022/11/19 14:15
     * @param prefix 前缀
     * @return java.lang.String 账号
     */
    public String generateBankAccountNo(String prefix);


    public String generateBankAccountNo2(String prefix);
}
