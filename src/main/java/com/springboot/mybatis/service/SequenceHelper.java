package com.springboot.mybatis.service;

import com.springboot.mybatis.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class SequenceHelper {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String idc;

    protected String getIdc() {
        if (StringUtils.isEmpty(idc)) {
            String IDC_SQL = "SELECT SEQ_PREFIX()";
            int IDC_LENGTH = 2;
            idc = StringUtils.leftFillZero(jdbcTemplate.queryForObject(IDC_SQL, String.class), IDC_LENGTH);
        }
        return idc;
    }
}
