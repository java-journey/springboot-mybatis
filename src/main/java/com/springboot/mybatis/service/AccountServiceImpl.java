/*
 * Copyright (C), 2002-, 苏宁易购电子商务有限公司
 * FileName:
 * Author:
 * Date:
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名     修改时间    版本号       描述
 */
package com.springboot.mybatis.service;

import com.springboot.mybatis.utils.StringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 *
 * @author sol
 * @date 2022/11/19 14:12
 * @return null
 */
@Service("accountServiceImpl")
public class AccountServiceImpl implements AccountService{

    @Autowired
    private SequenceService sequenceService;

    @Autowired
    private SequenceDaoHelper sequenceDaoHelper;


    @Autowired
    @Qualifier("accountTypeBean")
    private HashMap accountType;

    @Override
    public String generateBankAccountNo(String prefix) {
        System.out.println("prefix :"+prefix);
        int sequence = sequenceService.getBankAccountSerialNoId();
        prefix= (String) accountType.get(prefix);
        System.out.println("prefix :"+prefix);

        System.out.println("sequenceService :"+sequence);
       // sequence=Math.abs(sequence);
        String tem=  StringUtils.generator(sequence,6,prefix);
        System.out.println("StringUtils.generator :"+tem);
        return tem;
    }


    @Override
    public String generateBankAccountNo2(String prefix) {


        return sequenceDaoHelper.getFcpFileControlBizNo();
    }
}
