package com.springboot.mybatis.service;

import com.springboot.mybatis.utils.StringUtils;
import com.springboot.mybatis.utils.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("sequenceDaoHelper")
public class SequenceDaoHelper extends SequenceHelper{
    private static final Logger LOGGER = LoggerFactory.getLogger(SequenceDaoHelper.class);
    private static final String CLEAR_ORDER_NO_SQL = "SELECT next value for MYCATSEQ_SEQ_FCP_COPPER_COIN_CLEAR_DETAIL_NO ";

    private static final String FCP_FILE_CONTROL_BIZ_NO_SQL = "SELECT next value for MYCATSEQ_SEQ_FCP_FILE_CONTROL_BIZ_NO ";

    private static final String FCP_FILE_REQUEST_SERIAL_CONTROL_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_FTP_REQUEST_SERIAL_CONTROL_NO ";

    private static final String FCP_LCC_TO_SETTLE_FILE_BATCH_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_LCC_TO_SETTLE_FILE_BATCH_NO ";

    private static final String FCP_LCC_CLEAR_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_LCC_CLEAR_ORDER_NO ";

    private static final String FCP_LCC_CLEAR_CONTROL_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_LCC_CLEAR_CONTROL_NO ";

    private static final String FCP_SEG_DISCOUNT_INTEREST_CLEAR_NO="SELECT next value for MYCATSEQ_SEQ_FCP_SEG_DISCOUNT_INTEREST_CLEAR_NO ";

    /**
     * 酒店明细单号
     */
    private static final String FCP_HOTEL_CLEAR_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_HOTEL_CLEAR_ORDER_NO ";

    /**
     * 保险电销明细单号
     */
    private static final String FCP_INSURANCE_CLEAR_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_INSURANCE_CLEAR_ORDER_NO ";

    private static final String SEQ_FCP_FEE_CLEAR_CONTROL_CLEAR_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_FEE_CLEAR_CONTROL_CLEAR_ORDER_NO ";

    private static final String SEQ_FCP_FEE_TO_SETTLE_CONTROL_TO_FILE_BATCH_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_FEE_TO_SETTLE_CONTROL_TO_FILE_BATCH_NO ";

    private static final String SEQ_FCP_COPPER_POUNDAGE_DETAIL_BIZ_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_COPPER_POUNDAGE_DETAIL_BIZ_ORDER_NO ";
    /**宁互宝orderNo**/
    private static final String SEQ_NING_HU_BAO_DETAIL_BIZ_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_NING_HU_BAO_DETAIL_BIZ_ORDER_NO ";
    /**向上外催服务费orderNo**/
    private static final String SEQ_LCC_UP_DETAIL_BIZ_ORDER_NO = "SELECT next value for MYCATSEQ_SEQ_LCC_UP_CLEAR_DETAIL_BIZ_ORDER_NO";
    //拒贷引流
    private static final String SEQ_REFUSE_LOAN_CLEAR_DETAIL_NO = "SELECT next value for MYCATSEQ_SEQ_REFUSE_LOAN_CLEAR_DETAIL_NO ";
    //汽车助贷
    private static final String SEQ_AUTO_LOAN_CLEAR_DETAIL_NO = "SELECT next value for MYCATSEQ_SEQ_AUTO_LOAN_CLEAR_DETAIL_NO ";
    //理财收入
    private static final String SEQ_FINANCIAL_INCOME_CLEAR_DETAIL_NO = "SELECT next value for MYCATSEQ_SEQ_FINANCIAL_INCOME_CLEAR_NO ";
    //ABS清算单号
    private static final String SEQ_FCP_ABS_CLEAR_DETAILS_CLEAR_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_ABS_CLEAR_DETAILS_CLEAR_NO ";
    //通用的
    private static final String SEQ_CLEAR_GENERIC_DETAIL_CLEAR_NO = "SELECT next value for MYCATSEQ_SEQ_CLEAR_GENERIC_DETAIL_CLEAR_NO ";
    /**
     * 理财收入
     */
    private static final String FCP_FINANCIAL_INCOME_TO_BATCH_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_FINANCIAL_INCOME_TO_BATCH_NO ";
    /**
     *  联合营销
     */
    private static final String FCP_LHYX_CLEAR_NO = "SELECT next value for MYCATSEQ_SEQ_FCP_LHYX_CLEAR_NO ";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String getFcpFileControlBizNo() {
        Date date = new Date();
        StringBuilder sb = new StringBuilder("FILE");
        try {
            String seq = StringUtils.leftFillZero(jdbcTemplate.queryForObject(FCP_FILE_CONTROL_BIZ_NO_SQL, String.class), 16);

            //sb.append(DateUtil.getShortStrDate(date));
            sb.append(seq);
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }


        String bizNo = sb.toString();
        LOGGER.debug("生成fileControlBizNo:{}", bizNo);
        return bizNo;
    }

    private String getTableSubffix(String tableIndex) {
        if (StringUtils.isEmpty(tableIndex)) {
            return "";
        }
        return String.valueOf(Math.abs(tableIndex.hashCode() % 1000 % 32));
    }


    public String getFcpLcClearOrderNo(String bizType, String bpNo){
        Date date = new Date();
        String seq = StringUtils.leftFillZero(jdbcTemplate.queryForObject(FCP_LCC_CLEAR_ORDER_NO, String.class), 16);
        StringBuilder sb = new StringBuilder();
        //yyyyMMdd 8
        sb.append(DateUtil.getShortStrDate(date));


        //业务类型 4
        sb.append(StringUtils.leftFillZero(bizType, 5));
        //表后缀 4
        sb.append(StringUtils.leftFillZero(getTableSubffix(bpNo), 3));
        sb.append(seq);
        String toSettleBatchNo = sb.toString();
        LOGGER.debug("生成催收佣金明细唯一单号:{}", toSettleBatchNo);
        return toSettleBatchNo;
    }

}
