/*
 * Copyright (C), 2002-,
 * FileName:
 * Author:
 * Date:
 * Description: //模块目的、功能描述
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名     修改时间    版本号       描述
 */
package com.springboot.mybatis.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.support.incrementer.MySQLMaxValueIncrementer;
import org.springframework.stereotype.Service;


/**
 * @author sol
 */
@Service("sequenceService")
public class SequenceService {

    @Autowired
    @Qualifier("bankAccountSerialNoGenarater")
    private MySQLMaxValueIncrementer bankAccountSerialNoGenarater;

    public int getBankAccountSerialNoId() {
        return bankAccountSerialNoGenarater.nextIntValue();
    }

    public String getBankAccountSerialNoId2() {
        return bankAccountSerialNoGenarater.nextStringValue();
    }

}
