package com.springboot.mybatis.service;

import com.springboot.mybatis.domain.Employees;
import com.springboot.mybatis.mapper.EmployeesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EmployService {

    @Autowired(required=false)
    private EmployeesMapper employMapper;


    List<Employees> getEmployeesList(List<String> employeeIds){

     return  employMapper.getEmployeesListParams(employeeIds);

    }


    List<Employees> getEmployeesArray(String[] employeeIds){
        return  employMapper.getEmployeesArrayParams(employeeIds);
    }

    List<Employees> getEmployeesMap(Map<String, Object> params){
        return  employMapper.getEmployeesMapParams(params);
    }

}
