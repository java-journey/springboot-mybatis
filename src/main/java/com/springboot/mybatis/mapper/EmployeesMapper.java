package com.springboot.mybatis.mapper;

import com.springboot.mybatis.domain.Employees;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface EmployeesMapper {

    List<Employees> getEmployeesListParams(List<String> employeeIds);

    List<Employees> getEmployeesArrayParams(String[] employeeIds);

    List<Employees> getEmployeesMapParams(Map<String, Object> params);
}