package com.springboot.mybatis.lang;


/**
 * 回调接口
 *
 * @author sol 2010-11-11
 */
public interface CallBack<T> {

    /**
     * 调用
     * @return 结果
     */
    T invoke();

}

