package com.springboot.mybatis.lang;



/**
 * 应用异常
 *
 * @author sol 2010-10-1
 */
public class AppException extends RuntimeException {

    private static final long serialVersionUID = 153638716139197508L;

    /** 错误码 */
    protected String          errorCode;

    /**
     * 构造函数
     * @param errorCode 错误码
     */
    public AppException(String errorCode) {
        super(errorCode);
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 异常信息
     */
    public AppException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param cause 原异常
     */
    public AppException(String errorCode, Throwable cause) {
        super(errorCode, cause);
        this.errorCode = errorCode;
    }

    /**
     * 构造函数
     * @param errorCode 错误码
     * @param message 异常信息
     * @param cause 原异常
     */
    public AppException(String errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * 获取错误码
     * @return 错误码
     */
    public String getErrorCode() {
        return errorCode;
    }

}

