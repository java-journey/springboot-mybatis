package com.springboot.mybatis.lang;

/**
 * 错误码
 *
 * @author sol 2010-7-2
 */
public class ErrorCode {

    /** 成功 */
    public static final String SUCCESS                                 = "success";

    /** 失败 */
    public static final String FAIL                                    = "fail";

    /** 系统异常 */
    public static final String ERROR_SYSTEM                            = "error.system";

    /** 远程调用异常 */
    public static final String ERROR_REMOTE_INVOKE                     = "error.remote.invoke";

}

