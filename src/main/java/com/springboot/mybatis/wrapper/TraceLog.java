/*
 * Copyright (C), 2002-2014, 苏宁易购电子商务有限公司
 * FileName: TraceLog.java
 * Author:   12075176
 * Date:     2014-8-20 下午3:19:59
 * Description: //模块目的、功能描述      
 * History: //修改记录
 * <author>      <time>      <version>    <desc>
 * 修改人姓名             修改时间            版本号                  描述
 */
package com.springboot.mybatis.wrapper;


import com.springboot.mybatis.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 * 
 * @author sol
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
public class TraceLog {

    /** logger */
    protected Logger logger;

    /** 调用方法名 */
    protected String method;

    /** 调用时间阈值 */
    protected long threshold = 0L;

    /** 调用流水号 */
    protected String invokeNo;

    /** 异常类型 */
    protected String exType = "N";

    /** 超过调用时间阈值 */
    protected String beyondThd = "N";

    /** 开始时间 */
    protected long beginTime;

    /** 结束时间 */
    protected long endTime;

    /** 日志信息 */
    protected StringBuilder message = new StringBuilder();

    private static final String INVOKE_NO = "invokeNo";

    /**
     * 构造函数
     * 
     * @param logger logger
     * @param method 调用方法名
     * @param threshold 调用时间阈值
     */
    public TraceLog(Logger logger, String method, long threshold) {
        this.logger = logger;
        this.method = method;
        this.threshold = threshold;
        this.invokeNo = UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * 开始
     */
    public void begin() {
        this.beginTime = System.currentTimeMillis();

        MDC.put(INVOKE_NO, UUID.randomUUID().toString().replace("-", ""));
    }

    /**
     * 结束
     */
    public void end() {
        if (this.logger.isWarnEnabled()) {
            this.endTime = System.currentTimeMillis();
            long runTime = this.endTime - this.beginTime;

            if (threshold > 0L && runTime > threshold) {
                this.beyondThd = "Y";
            }

            // 日志格式：方法名|执行时间（毫秒）|是否超过阈值|异常类型
            // 日志示例：ME:CardBiz.register|RT:1211|BT:Y|ET:N
            this.message.append("ME:").append(this.method)
                    .append("|RT:").append(runTime)
                    .append("|BT:").append(this.beyondThd)
                    .append("|ET:").append(this.exType);
        }
    }

    /**
     * 重置
     * 
     * @param method 调用方法名
     * @param threshold 调用时间阈值
     */
    public void reset(String method, long threshold) {
        this.method = method;
        this.threshold = threshold;
        this.exType = "N";
        this.beyondThd = "N";
    }

    /**
     * 设置异常类型
     * 
     * @param exType 异常类型
     */
    public void setExType(String exType) {
        this.exType = exType;
    }

    /**
     * 记日志
     */
    public void log() {
        try {
            if (this.logger.isWarnEnabled()) {
                this.logger.warn(this.message.toString());
            }
        } finally {

            MDC.remove(Constants.TRACE_ID);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.message.toString();
    }
}
