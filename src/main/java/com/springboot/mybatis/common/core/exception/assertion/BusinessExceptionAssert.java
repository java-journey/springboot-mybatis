package com.springboot.mybatis.common.core.exception.assertion;

import com.springboot.mybatis.common.core.constant.IResponseEnum;
import com.springboot.mybatis.common.core.exception.BaseException;
import com.springboot.mybatis.common.core.exception.BusinessException;

import java.text.MessageFormat;

public interface BusinessExceptionAssert extends IResponseEnum, Assert {

    @Override
    default BaseException newException(Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this, args, msg);
    }

    @Override
    default BaseException newException(Throwable cause, Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this, args, msg, cause);
    }
}
