package com.springboot.mybatis.common.core.pojo.request;

import com.sun.istack.internal.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class QuerRequest  extends BaseRequest {
    @NotNull
    private String name;
    @NotNull
    private Integer age;
    private Boolean gender;
    // 省略getter, setter方法
}
