package com.springboot.mybatis.common.core.exception;

/**
 * 只包装了 错误信息 的 {@link RuntimeException}.
 * 用于 {@link com.springboot.mybatis.common.core.exception.assertion.Assert} 中用于包装自定义异常信息
 *
 * @author sol
 * @date 2018/10/20
 */
public class WrapMessageException extends RuntimeException {

    private static final long serialVersionUID = 8136984331956330614L;

    public WrapMessageException(String message) {
        super(message);
    }

    public WrapMessageException(String message, Throwable cause) {
        super(message, cause);
    }

}
