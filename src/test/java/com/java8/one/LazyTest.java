package com.java8.one;

public class LazyTest {
    public static void main(String[] args) {
        Lazy<Integer> a = Lazy.of(() -> 10 + 1);
        int b = a.get() + 1;
        // get 不会再重新计算, 直接用缓存的值
        int c = a.get();
    }
}
