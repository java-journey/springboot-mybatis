package com.springboot.mybatis.csv;



import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

import java.io.Serializable;


@Data
public class MockRequest implements Serializable {

    private static final long serialVersionUID = 2196752795120873210L;
    /**
     * 商户号
     */
    @CsvBindByPosition(position = 0)
    private String bpNo;


    /**
     * 业务类型
     * 接收上游的业务类型  固定值101
     * @see ClearBizTypeEnum
     */
    @CsvBindByPosition(position = 1)
    private String bizType="101";

    /**
     * 结算金额，单位：元（最多保留2位小数）
     */
    @CsvBindByPosition(position = 2)
    private String transAmout;

    /**
     * 收入支出标识-固定值01
     */
    @CsvBindByPosition(position = 3)
    private String inExpFlag="01";


    /**
     * 交易日期 格式：YYYY-MM-DD，业务发生日期
     */
    @CsvBindByPosition(position = 4)
    private String transDate;

    /**
     * 明细id-结算明细主键，唯一不可重复
     */
    @CsvBindByPosition(position = 5)
    private String detailId;

    /**
     * 产品编码 固定值01010207（表示‘外部商户D0服务费’）
     */
    @CsvBindByPosition(position = 6)
    private String productCode="01010207";

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
        throw new java.io.NotSerializableException("com.springboot.mybatis.csv.MockRequest");
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
        throw new java.io.NotSerializableException("com.springboot.mybatis.csv.MockRequest");
    }
}


