package com.springboot.mybatis.csv;


import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSON;
import com.opencsv.ICSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import com.springboot.mybatis.ResourceHelper;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.Writer;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 〈〉<br>
 * 〈功能详细描述〉
 *
 * @author sol
 * @Date: Created in 10:26 2018/11/23
 * @see [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Slf4j
@SpringBootTest(classes=RequestDataMockTest.class)
public class RequestDataMockTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestDataMockTest.class);

    List<MockRequest> requestList= new ArrayList<>();

    public  void before() {
        DateTimeFormatter yyy_MM_dd = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startInclusive=LocalDate.parse("2022-11-01");
        LocalDate endExclusive=LocalDate.parse("2022-11-30");
        long star =startInclusive.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        long end =endExclusive.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli();
        for (int i = 0; i <50 ; i++) {
            MockRequest request=new MockRequest();
            request.setDetailId(generateKeys());
            request.setBpNo(generateBPNoKeys());
            request.setTransAmout(generateAmount());
            request.setTransDate( between(star,end));
            requestList.add(request);
        }

    }
    @Test
    public void toText(){
        String text = ResourceHelper.getResourceAsString(getClass(), "com.springboot.mybatis.testJson.withCreate" + "requestCreate.json");
    }

    @Test
    public void json()  {
        before();
        System.out.println(JSON.toJSONString(requestList));
        log.info("userList={}",JSON.toJSONString(requestList));
    }

    @Test
    public void execute()  {
        String filePath="request.csv";
        before();
        List<MockRequest> list= new ArrayList<>();

        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(MockRequest.class);


        try {

            @Cleanup
            Writer writer = new FileWriterWithEncoding(filePath, CharsetUtil.CHARSET_GBK);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
                    .withSeparator(ICSVWriter.DEFAULT_SEPARATOR) // 分隔符
                    .withQuotechar(ICSVWriter.NO_QUOTE_CHARACTER) // 不使用引号
                    .withMappingStrategy(strategy) // 映射策略
                    .build();
            beanToCsv.write(requestList);
        } catch (CsvDataTypeMismatchException e) {
            LOGGER.error("CsvDataTypeMismatchException :{}",e.getMessage());
            e.printStackTrace();

        } catch (CsvRequiredFieldEmptyException e) {
            LOGGER.error("CsvRequiredFieldEmptyException :{}",e.getMessage());
            e.printStackTrace();
        }catch (Exception e) {
            LOGGER.error("Exception :{}",e.getMessage());
            e.printStackTrace();
        }


    }

    /**
     *
     * @param startInclusive 开始时间-date
     * @param endExclusive 结束时间-date
     * @return  区间内随机日期
     */

    public Date between(Date startInclusive, Date endExclusive) {
        long startMillis = startInclusive.getTime();
        long endMillis = endExclusive.getTime();
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis, endMillis);
        return new Date(randomMillisSinceEpoch);
    }
    /**
     * 根据long型-开始-结束时间 生成时间值
     * @param startMillis 开始时间-long
     * @param endMillis 结束时间-long
     * @return 区间内随机日期
     */
    public  String between(long startMillis, long endMillis) {
        long randomMillisSinceEpoch = ThreadLocalRandom
                .current()
                .nextLong(startMillis, endMillis);
        LocalDate local = LocalDateTime.ofInstant(Instant.ofEpochMilli(randomMillisSinceEpoch), ZoneId.systemDefault()).toLocalDate();
        DateTimeFormatter yyy_MM_dd = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        return local.format(yyy_MM_dd);
    }

    public static String generateKeys(){
        String key;
        char[] keyChar = {
                'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '0','1','2','3','4','5','6','7','8','9'
        };
        String keyHead = RandomStringUtils.random(9, keyChar);

        LocalDateTime localDateTime=LocalDateTime.now();
        DateTimeFormatter yyy_MM_dd = DateTimeFormatter.ofPattern("yyyyMMddhhmmss");
        String dateStr = yyy_MM_dd.format(localDateTime);

        String keyTail = RandomStringUtils.random(9, keyChar);
        keyTail="";
        key = keyHead+dateStr+keyTail;
        return key;
    }

    public static String generateBPNoKeys(){
        String key;
        char[] keyChar = {
                'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'

        };
        char[] noChar = {
                '0','1','2','3','4','5','6','7','8','9'
        };
        String keyHead = RandomStringUtils.random(2, keyChar);

        String keyTail = RandomStringUtils.random(6, noChar);
        key = keyHead+keyTail;
        return key;
    }

    public static String generateAmount(){

        long min_val = 10L;
        long max_val = 1000000L;
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        long randomNum = tlr.nextLong(min_val, max_val + 1);
        System.out.println("Random Number: "+randomNum);

        return randomNum+"";
    }




}