package com.springboot.mybatis.service;

import com.alibaba.fastjson.JSON;
import com.springboot.mybatis.domain.UserDO;
import com.springboot.mybatis.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户DAO测试类
 */
@Slf4j
@RunWith(PowerMockRunner.class)
@SpringBootTest(classes = {UserDaoTest.class})
public class UserDaoTest {

    /** 用户DAO */
   // @Resource
  //  private UserDAO userDAO;

    @Mock
    private UserMapper userDAO;

    /**
     * 测试: 根据公司标识查询
     */
    @Test
    public void testQueryByCompanyId() {
        Long companyId = 1L;
     //   List<UserDO> userList = userDAO.queryByCompanyId(companyId);
        List<UserDO> userList =new ArrayList<>();
        log.info("userList={}", JSON.toJSONString(userList));
    }

}
