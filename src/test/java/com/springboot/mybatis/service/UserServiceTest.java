package com.springboot.mybatis.service;

import com.alibaba.fastjson.JSON;
import com.springboot.mybatis.ResourceHelper;
import com.springboot.mybatis.domain.UserDO;
import com.springboot.mybatis.domain.UserVO;
import com.springboot.mybatis.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.util.IdGenerator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * 用户服务测试类
 */
@RunWith(PowerMockRunner.class)
public class UserServiceTest {

    /** 用户DAO */
    @Mock
    private UserMapper userDAO;
    /** 标识生成器 */
    @Mock
    private IdGenerator idGenerator;

    /** 用户服务 */
    @InjectMocks
    private UService userService;

    /** 资源路径 */
    private static final String RESOURCE_PATH = "testUserService/";

    /**
     * 在测试之前
     */
    @BeforeEach
    public void beforeTest() {
        // 注入依赖对象
        Whitebox.setInternalState(userService, "canModify", Boolean.TRUE);
    }

    /**
     * 测试: 创建用户-创建
     */
    @Test
    public void testCreateUserWithCreate() {
        // 模拟依赖方法
        // 模拟依赖方法: userDAO.getByName
        Mockito.doReturn(null).when(userDAO).getIdByName(Mockito.anyString());
        // 模拟依赖方法: idGenerator.next
        Long userId = 1L;
        Mockito.doReturn(userId).when(idGenerator).generateId();

        // 调用测试方法
        String path = RESOURCE_PATH + "testCreateUserWithCreate/";
        String text = ResourceHelper.getResourceAsString(getClass(), path + "userCreateVO.json");
        UserVO userCreate = JSON.parseObject(text, UserVO.class);
      assertEquals( userId, userService.createUser(userCreate),"用户标识不一致");

        // 验证依赖方法
        // 验证依赖方法: userDAO.getByName
        Mockito.verify(userDAO).getIdByName(userCreate.getName());
        // 验证依赖方法: idGenerator.next
        Mockito.verify(idGenerator).generateId();
        // 验证依赖方法: userDAO.create
        ArgumentCaptor<UserDO> userCreateCaptor = ArgumentCaptor.forClass(UserDO.class);
        Mockito.verify(userDAO).create(userCreateCaptor.capture());
        text = ResourceHelper.getResourceAsString(getClass(), path + "userCreateDO.json");
        assertEquals( text, JSON.toJSONString(userCreateCaptor.getValue()),"用户创建不一致");

        // 验证依赖对象
        // 验证依赖对象: idGenerator, userDAO
        Mockito.verifyNoMoreInteractions(idGenerator, userDAO);
    }

    /**
     * 测试: 创建用户-修改
     */
    @Test
    public void testCreateUserWithModify() {
        // 模拟依赖方法
        // 模拟依赖方法: userDAO.getByName
        Long userId = 1L;
        Mockito.doReturn(userId).when(userDAO).getIdByName(Mockito.anyString());

        // 调用测试方法
        String path = RESOURCE_PATH + "testCreateUserWithModify/";
        String text = ResourceHelper.getResourceAsString(getClass(), path + "userCreateVO.json");
        UserVO userCreate = JSON.parseObject(text, UserVO.class);
        assertEquals(userId, userService.createUser(userCreate),"用户标识不一致");

        // 验证依赖方法
        // 验证依赖方法: userDAO.getByName
        Mockito.verify(userDAO).getIdByName(userCreate.getName());
        // 验证依赖方法: userDAO.modify
        ArgumentCaptor<UserDO> userModifyCaptor = ArgumentCaptor.forClass(UserDO.class);
        Mockito.verify(userDAO).modify(userModifyCaptor.capture());
        text = ResourceHelper.getResourceAsString(getClass(), path + "userModifyDO.json");
        assertEquals(text, JSON.toJSONString(userModifyCaptor.getValue()),"用户修改不一致");

        // 验证依赖对象
        // 验证依赖对象: idGenerator
        Mockito.verifyNoInteractions(idGenerator);

      //  Mockito.
        // 验证依赖对象: userDAO
        Mockito.verifyNoMoreInteractions(userDAO);
    }

    /**
     * 测试: 创建用户-异常
     */
    @Test
    public void testCreateUserWithException() {
        // 注入依赖对象
        Whitebox.setInternalState(userService, "canModify", Boolean.FALSE);

        // 模拟依赖方法
        // 模拟依赖方法: userDAO.getByName
        Long userId = 1L;
        Mockito.doReturn(userId).when(userDAO).getIdByName(Mockito.anyString());

        // 调用测试方法
        String path = RESOURCE_PATH + "testCreateUserWithException/";
        String text = ResourceHelper.getResourceAsString(getClass(), path + "userCreateVO.json");
        UserVO userCreate = JSON.parseObject(text, UserVO.class);
        UnsupportedOperationException exception = assertThrows(
                UnsupportedOperationException.class, () -> userService.createUser(userCreate),"返回异常不一致");
        assertEquals("不支持修改", exception.getMessage(),"异常消息不一致");

        // 验证依赖方法
        // 验证依赖方法: userDAO.getByName
        Mockito.verify(userDAO).getIdByName(userCreate.getName());

        // 验证依赖对象
        // 验证依赖对象: idGenerator
        Mockito.verifyNoInteractions(idGenerator);
        // 验证依赖对象: userDAO
        Mockito.verifyNoMoreInteractions(userDAO);
    }

}
