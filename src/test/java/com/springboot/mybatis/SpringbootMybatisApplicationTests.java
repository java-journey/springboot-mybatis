package com.springboot.mybatis;

import cn.hutool.core.util.CharsetUtil;
import com.opencsv.CSVWriterBuilder;
import com.opencsv.ICSVWriter;
import lombok.Cleanup;
import org.apache.commons.io.output.FileWriterWithEncoding;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class SpringbootMybatisApplicationTests {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringbootMybatisApplicationTests.class);

	@Test
	void contextLoads() {
	}


	@Test
	public void csvTest(){
		String filePath="test.csv";
		List<String[]> list = new ArrayList<>();
		list.add(new String[]{"id", "short_name", "name", "remark", "parent_id", "type_name", "type_id"});
		list.add(new String[]{"1", "", "大型汽车号牌", "1.00", "", "号牌种类", "1"});
		list.add(new String[]{"2", "", "小型汽车号牌", "2.00", "", "号牌种类", "1"});
		list.add(new String[]{"3", "", "使馆汽车号牌", "3.50", "", "号牌种类", "1"});


		try {
			@Cleanup
			ICSVWriter icsvWriter = new CSVWriterBuilder(new FileWriterWithEncoding(filePath, CharsetUtil.CHARSET_UTF_8))
					.withSeparator(ICSVWriter.DEFAULT_SEPARATOR) // 分隔符
					.withQuoteChar(ICSVWriter.NO_QUOTE_CHARACTER) // 不使用引号
					.build();
			icsvWriter.writeAll(list);
		}catch (Exception e){
			LOGGER.error(" Exception is :  {}",e.getMessage());

		}


	}


}
